// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/access/Ownable.sol";
import "./interfaces/IArcadeToken.sol";

contract Tetris is Ownable{

    //Account Structure for their own game
    struct accountGame {
        address connectedAccount1;
        address connectedAccount2;
        uint256 betAmount;
    }
    
    IArcadeToken tokenAddr;

    //Store the accounts as the game ID
    mapping(uint => accountGame) public gameControl;

    //Store the amount following address to the betANTamount
    mapping(address => uint256) public betANTamount;

    //Using isbetting mapping for the flag of the bet performance
    mapping(address => bool) public isbetting;

    //Contract comision
    uint16 feePercent;

    //Event return token to the Owner
    event ReturnToOwner(uint256 amount);

    //Event return token to the Winner
    event WithDraw(address winner, uint256 amount);

    //transaction successfully performed and receive ANT
    event Received (address , uint);
    
    constructor(IArcadeToken _tokenAddr, uint8 _feePercent) {
        tokenAddr = _tokenAddr;
        feePercent = _feePercent;
    }

    //Return the GameState as the GameID
    function getGameState(uint gameID) public view returns(address account1, address account2, uint256 amount ) {
        accountGame memory accountGameById = gameControl[gameID];
        return (accountGameById.connectedAccount1, accountGameById.connectedAccount2, accountGameById.betAmount);
    }
    
    //Create the Game and Set GameID
    function createGame(uint gameID, address account1, address account2) public returns(bool) {
        accountGame memory newGame = accountGame(account1, account2, 0);
        gameControl[gameID] = newGame;
        return true;
    }
    //Transform the ANT from account to the SC
    function transferANT(uint256 gameID, uint256 _tokenAmount) public returns(bool){
        gameControl[gameID].betAmount += _tokenAmount;
        betANTamount[msg.sender] = _tokenAmount;
        tokenAddr.transferFrom(msg.sender, address(this), _tokenAmount);
        emit Received(address(this), _tokenAmount);
        return true;
    }
    
    //Transform all of the SC's ANT.---for the test deploy.
    function returnToOwner() public{
        tokenAddr.transferFrom(address(this), owner(), address(this).balance);
        emit ReturnToOwner(address(this).balance);
    }

    //Return the ANT to the winner, some comision to the owner.
    function withdraw(uint gameID) external payable {
        tokenAddr.transfer(msg.sender, gameControl[gameID].betAmount*(100-feePercent)/(100));
        tokenAddr.transfer(owner(),  gameControl[gameID].betAmount*feePercent/(100));
        emit WithDraw(msg.sender, gameControl[gameID].betAmount*(100-feePercent)/(100));
    }
}
